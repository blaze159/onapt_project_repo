<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Profile Page</title>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<spring:url value="/resources/css/style.css"/>"  type="text/css"/>

  
  </head>

  <body>

   <jsp:include page="../views/fragments/header.jsp"></jsp:include>
    <section>
         <div class="container container-main">
          <img src="<spring:url value="/resources/img/person.png"/>" class="profile-img"/>
         <header>
               <h1><strong>John Doe</strong></h1>
         </header>
         <img src="<spring:url value="/resources/img/line.png"/>" class="line"/>
         <div class="row">
         <div class="col-md-6 about-me">
           <h2>#About Me</h2>
           <p>Name in Ukraine: <span>Джон Доу</span></p>
           <p>Name in Russian: <span>Джон Доу</span></p>
           <p>Science degree: </p>
           <p>Academic title: </p>
           <p>Position: </p>
           <p>Year of birth: </p>
         </div>
         <div class="col-md-6">
         <h2># Contact Information</h2>
            <ul class="contact-list">
              <li><img src="<spring:url value="/resources/img/phone.png"/>"> 07444 107695</li>
              <li><img src="<spring:url value="/resources/img/email.png"/>"> blaze159@narod.ru</li>
              <li><img src="<spring:url value="/resources/img/orcid-logo.png"/>"> Orcid <span>orcid.com/blaze159</span></li>
              <li><img src="<spring:url value="/resources/img/google-scholar.png"/>"> Google Scholar <span>google-scholar.com/blaze159</span></li>
            </ul>
         </div>
         </div>
          <div class="row">
          <div class="col-md-12">
            <h2># Publications</h2>

            <div class="media">
              <div class="media-left">
                1
              </div>
              <div class="media-body">
                <h4 class="media-heading">Eurosoft (UK) Ltd</h4>
                <p>The Federation's gone; the Borg is everywhere! Computer, lights up!
                  Is it my imagination, or have tempers become a little frayed on the
                  ship lately?</p>
              </div>
            </div>

            <div class="media">
              <div class="media-left">
                2
              </div>
              <div class="media-body">
                <h4 class="media-heading">EDS (Defence) Ltd</h4>
                <p>Yes, absolutely, I do indeed concur, wholeheartedly! About four years.
                  I got tired of hearing how young I looked. Congratulations - you just destroyed
                  the Enterprise.</p>
              </div>
            </div>

            <div class="media">
              <div class="media-left">
                3
              </div>
              <div class="media-body">
                <h4 class="media-heading">McCue Plc</h4>
                <p>I'll be sure to note that in my log. Worf, It's better than music. It's jazz.
                  Travel time to the nearest starbase? I am your worst nightmare! Not if I weaken first.
                  Computer, belay that order. I guess it's better to be lucky than good.</p>
              </div>
            </div>

            <div class="media">
              <div class="media-left">
                4
              </div>
              <div class="media-body">
                <h4 class="media-heading">Ericsson Mobile Platforms</h4>
                <p>About four years. I got tired of hearing how young I looked. Ensign Babyface!
                  I think you've let your personal feelings cloud your judgement. I recommend you
                  don't fire until you're within 40,000 kilometers.</p>
              </div>
            </div>

            <div class="media">
              <div class="media-left">
                5
              </div>
              <div class="media-body">
                <h4 class="media-heading">Switch Concepts</h4>
                <p>Earl Grey tea, watercress sandwiches... and Bularian canapés? Are you up for
                  promotion? I've had twelve years to think about it. And if I had it to do
                  over again, I would have grabbed the phaser and pointed it at you instead
                  of them.</p>
              </div>
            </div>
          </div>
        
      </div>
      
  </div>
    </section>
     
    <footer>
      <div class="container">
        <p>Copyright &copy; ONAPT 2016</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<spring:url value="/resources/js/bootstrap.js"/>"></script>
    
  </body>
</html>
